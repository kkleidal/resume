First you need to install node.js and npm.

Then from this directory run:
```
npm install .
mkdir -p build
node compile.js
firefox build/no-pic.html   # or the browser of your choice
```
