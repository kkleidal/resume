'use-strict';

var fs = require('fs'),
	async = require('async'),
	Handlebars = require('handlebars');

// Function found at http://stackoverflow.com/questions/11293857/fastest-way-to-copy-file-in-node-js
function copyFile(source, target, cb) {
	var cbCalled = false;

	var rd = fs.createReadStream(source);
		rd.on("error", function (err) {
		done(err);
	});
	var wr = fs.createWriteStream(target);
	wr.on("error", function (err) {
		done(err);
	});
	wr.on("close", function (ex) {
		done();
	});
	rd.pipe(wr);

	function done(err) {
		if (!cbCalled) {
			cb(err);
			cbCalled = true;
		}
	}
}

function copyDirectoryContents( sourcePath, destPath, recursive, cb ) {
	function validatePath( aPath ) {
		if ( typeof aPath !== "string" || aPath.length === 0 ) {
			cb( new Error( "Path must be a string of non-zero length." ) );
			return;
		}
		if ( aPath.charAt( aPath.length - 1 ) !== '/' ) aPath += '/';
		return aPath;
	}
	sourcePath = validatePath( sourcePath );
	destPath = validatePath( destPath );
	fs.readdir( sourcePath, function ( err, files ) {
		if ( err ) {
			cb( err );
			return;
		}
		async.each( files, function ( file, cb2 ) {
			setImmediate( function () {
				fs.stat( sourcePath + file, function ( err, stats ) {
					if ( stats.isDirectory() ) {
						fs.mkdir( destPath + file, function ( err ) {
							if ( err ) {
								cb2( err );
								return;
							}
							if ( recursive ) {
								copyDirectoryContents( sourcePath + file, destPath + file, recursive, cb2 );
							} else {
								cb2();
							}
						} );
						return;
					}
					copyFile( sourcePath + file, destPath + file, cb2 );
				} );
			});
		}, function ( err ) {
			cb( err );
		});
	});
}

function removeDirectoryContents( sourcePath, recursive, cb ) {
	function validatePath( aPath ) {
		if ( typeof aPath !== "string" || aPath.length === 0 ) {
			cb( new Error( "Path must be a string of non-zero length." ) );
			return;
		}
		if ( aPath.charAt( aPath.length - 1 ) !== '/' ) aPath += '/';
		return aPath;
	}
	sourcePath = validatePath( sourcePath );
	fs.readdir( sourcePath, function ( err, files ) {
		if ( err ) {
			cb( err );
			return;
		}
		async.each( files, function ( file, cb2 ) {
			setImmediate( function () {
				fs.stat( sourcePath + file, function ( err, stats ) {
					if ( stats.isDirectory() ) {
						if ( recursive ) {
							removeDirectoryContents( sourcePath + file, recursive, function ( err ) {
								if ( err ) {
									cb2( err );
									return;
								}
								fs.rmdir( sourcePath + file, cb2 );
							} );
						} else {
							cb2();
						}
						return;
					}
					fs.unlink( sourcePath + file, cb2 );
				} );
			});
		}, function ( err ) {
			cb( err );
		});
	});
}

function removeAllFilesInBuild( cb_expected ) {
	var cb = arguments[ arguments.length - 1 ];
	removeDirectoryContents( './build', true, cb );
}

function copyPublicFiles( cb_expected ) {
	var cb = arguments[ arguments.length - 1 ]
	copyDirectoryContents( './public', './build', true, cb );
}

function readResume( cb_expected ) {
	var cb = arguments[ arguments.length - 1 ];
	fs.readFile( './resume.json', function ( err, contents ) {
		if ( err ) {
			cb( err );
			return;
		}
		var json;
		try {
			json = JSON.parse( contents.toString('utf-8') );
		} catch( e ) {
			cb( e );
			return;
		}
		cb( null, json );
	});
}

// TODO:  Write function which reads and compiles templates
function processTemplates( json, cb_expected ) {
	var cb = arguments[ arguments.length - 1 ];
	fs.readdir( './templates/', function ( err, files ) {
		if ( err ) {
			cb( err );
			return;
		}
		async.each( files, function ( file, cb1 ) {
			setImmediate( function() {

				var sourcePath = './templates/' + file;
				var destPath = './build/' + file.replace( /\.hbar$/, ".html" );
				console.log( sourcePath );
				function confirmFile( cb2 ) {
					fs.stat( sourcePath, function ( err, stats ) {
						if ( err ) {
							cb2( err );
							return;
						}
						if ( ! stats.isFile() ) {
							cb2( new Error( "Not a file." ) );
							return;
						}
						if ( ! /\.hbar$/.test( sourcePath ) ) {
							cb2( new Error( "Not a handlebars file (*.hbar)." ) );
							return;
						}
						cb2( null, sourcePath );
					} );
				}
				function applyTemplate( buffer, cb2 ) {
					setImmediate( function() {
						try {
							var template = Handlebars.compile( buffer.toString('utf-8') );
							var html = template( json );
							cb2( null, destPath, html );
						} catch( e ) {
							cb2( e );
						}
					});
				}
				async.waterfall( [ confirmFile, fs.readFile, applyTemplate, fs.writeFile ], function( err ) {
					if ( err ) {
						console.warn( err );
						// Warn and continue
					}
					cb1();
				} );

			});
		}, cb );
	});
}

function setupHandlebars() {
	Handlebars.registerHelper('join', function( context, joiner ) {
		return context.join( joiner );
	});
	Handlebars.registerHelper('ifNonEmpty', function(arr, options) {
		if ( Array.isArray( arr ) && arr.length > 0 ) {
			return options.fn(this);
		}
	});
	Handlebars.registerHelper('eachPercent', function(context, percentage, offset, options) {
		var ret = "";

		var start = Math.floor( context.length * ( offset / 100.0 ) );
		var number = Math.floor( context.length * ( percentage / 100.0 ) );
		var useRest = ( percentage + offset >= 100 );
		for(var i = start, j = context.length; i < j && ( useRest || i < start + number ); i++) {
			ret = ret + options.fn(context[i]);
		}

		return ret;
	});
	var reDate = /^([0-9]{4})-([0-1][0-9])-([0-3][0-9])$/;
	var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
	Handlebars.registerHelper('dateParse', function( context, full ) {
		var match = reDate.exec( context );
		if ( ! match ) {
			return context;
		}
		var year = parseInt( match[1], 10 ),
			month = parseInt( match[2], 10 ) - 1,
			date = parseInt( match[3], 10 );

		var dt = new Date( year, month, date );

		return months[ dt.getMonth() ] + ( full === true ? " " + dt.getDate().toString() : "" ) + " " + dt.getFullYear().toString()
	});
	Handlebars.registerHelper('toLower', function( context ) {
		return context.toLowerCase();
	});
}

function main() {
	setupHandlebars();
	async.waterfall( [ removeAllFilesInBuild, copyPublicFiles, readResume, processTemplates ], function( err ) {
		if ( err ) {
			console.error( err );
			return;
		}
		console.log( "Success!" );
	});
}

main();
